import requests
import json
import xmltodict

class ConvertXML:

    def request_xml_and_convert_to_dict(self, url):
        dict = None
        try:
            data = requests.get(url)
            dict = xmltodict.parse(data.content)
        except Exception as e:
            print 'gagal request atau parsing ke dict'
            print e
        return dict

    def convert_to_json(self, dict):
        json_data = None
        try:
            json_data = json.dumps(dict)
        except Exception as e:
            print 'gagal convert ke json'
            print e
        return json_data

    def save_to_file(self, json_data):
        try:
            print 'saving to file..'
            f = open('xml_to_json.json', 'w')
            f.write(json_data)
            f.close()
            print 'file saved...'
        except Exception as e:
            print 'gagal buat file'
            print e

if __name__ == '__main__':
    convertXML = ConvertXML()
    dict = convertXML.request_xml_and_convert_to_dict(url="http://www.redhat.com/security/data/oval/com.redhat.rhsa-all.xml")
    data_json = convertXML.convert_to_json(dict=dict)
    convertXML.save_to_file(json_data=data_json)